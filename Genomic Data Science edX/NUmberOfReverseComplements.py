from itertools import product


def ReverseComplement(Pattern):
    reverse =''
    for c in Pattern:
        if c == 'A':
            reverse+= 'T'
        elif c =='T':
            reverse+= 'A'
        elif c =='G':
            reverse+= 'C'
        elif c =='C':
            reverse+= 'G'
    reverse =reverse[::-1]
    return reverse

def NumberOfReverseComplements(k):
    allPossibleKmers = (''.join(i) for i in product('ATGC', repeat = k))
    count = 0
    for kmer in allPossibleKmers:
        if (ReverseComplement(kmer) == kmer):
            count += 1
            # print(kmer)
    return count

print(NumberOfReverseComplements(9))