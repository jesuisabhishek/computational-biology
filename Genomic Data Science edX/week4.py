import week3
import random

def RandomizedMotifSearch(Dna, k, t):
    Motifs = []
    for string in Dna:
        allKmersInString = [string[i:i+k] for i in range(len(string) -k+1)]
        Motifs.append(allKmersInString[random.randint(0,len(allKmersInString) -1)])
        BestMotifs  = Motifs
    # Test Case 
    BestMotifs = ['GTC','CCC','ATA','GCT']
    i=0
    while True:
        Profile =  week3.ConstructProfile(Motifs,k)
        Motifs = Motif(Profile, Dna,k)
        if week3.Score(Motifs,k) < week3.Score(BestMotifs,k):
            BestMotifs = Motifs
        else:
            return BestMotifs

def Motif(Profile, Dna,k):
    ret = []
    for string in Dna:
        ret.append(week3.ProfileMostProbableKmer(string,k,Profile))
    return ret

def RepeatedRandomizedMotifSearch(Dna, k, t,N):
    Motifs = []
    for string in Dna:
        allKmersInString = [string[i:i+k] for i in range(len(string) -k+1)]
        Motifs.append(allKmersInString[random.randint(0,len(allKmersInString) -1)])
        BestMotifs  = Motifs
    i = 0
    while i < N:
        Profile =  week3.ConstructProfile(Motifs,k)
        Motifs = Motif(Profile, Dna,k)
        if week3.Score(Motifs,k) < week3.Score(BestMotifs,k):
            BestMotifs = Motifs
        else:
            return BestMotifs

    
# print(RepeatedRandomizedMotifSearch(['CGCCCCTCTCGGGGGTGTTCAGTAAACGGCCA',
# 'GGGCGAGGTATGTGTAAGTGCCAAGGTGCCAG',
# 'TAGTACCGAGACCGAAAGAAGTATACAGGCGT',
# 'TAGATCAAGTTTCAGGTGCACGTCGGTGAACC',
# 'AATCCACCAGCTCCACGTGCAATGTTGGCCTA'],5,8,1000))



# Input: A dictionary Probabilities, where keys are k-mers and values are the probabilities of these k-mers (which do not necessarily sum up to 1)
# Output: A normalized dictionary where the probability of each k-mer was divided by the sum of all k-mers' probabilities
def Normalize(Probabilities):
    # your code here
    sum = 0
    for kmer in Probabilities:
        sum+=Probabilities[kmer]

    for kmer in Probabilities:
        Probabilities[kmer] /= sum
    return Probabilities 

# Input:  A dictionary Probabilities whose keys are k-mers and whose values are the probabilities of these kmers
# Output: A randomly chosen k-mer with respect to the values in Probabilities
def WeightedDie(Probabilities):
    
    # your code here
    x = round(random.uniform(0,1)*len(Probabilities))
    if(x < len(Probabilities)):
        return list(Probabilities)[x]
    else:
        return list(Probabilities)[x -1]
    

# print (WeightedDie(
# {
#     'AA':0.2,
#     'TT':0.2,
#     'CC':0.1,
#     'GG':0.1,
#     'AT':0.4
# }))
# def GibbsSampler(Dna, k, t, N):
#         randomly select k-mers Motifs = (Motif1, …, Motift) in each string from Dna
#         BestMotifs ← Motifs
#         for j ← 1 to N
#             i ← Random(t)
#             Profile ← profile matrix constructed from all strings in Motifs except for Motifi
#             Motifi ← Profile-randomly generated k-mer in the i-th sequence
#             if Score(Motifs) < Score(BestMotifs)
#                 BestMotifs ← Motifs
#         return BestMotifs


print(RepeatedRandomizedMotifSearch(['ATGAGGTC','GCCCTAGA','AAATAGAT','TTGTGCTA'],3,8,1))

# GTC CTA ATA CTA
# ATG TAG TAG TTG
# ATG CTA ATA CTA
# GTC GCC ATA GCT