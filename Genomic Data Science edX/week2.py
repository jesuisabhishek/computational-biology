from itertools import product
import week1


def MinimumSkew(Genome):
    skew =[]
    skew_i=0
    skew.append(skew_i)
    for nucleotide in Genome:
        if nucleotide == 'G':
            skew_i += 1
        elif nucleotide == 'C':
            skew_i -= 1
        skew.append(skew_i)
    # print (skew.index(max(skew)))
    minSkewList =[]
    minSkew = min(skew)
    for i in range(len(skew)):
        if skew[i] == minSkew:
            minSkewList.append(i)
    return minSkewList


def HammingDistance(p, q):
    hammingDistance =0
    for i in range(len(p)):
        if p[i] != q[i]:
            hammingDistance += 1
    return hammingDistance


def ApproximatePatternMatching(Text, Pattern, d):
    positions = [] # initializing list of positions
    allKmers = [Pattern[i:i+len(Text)] for i in range(len(Pattern)-len(Text)+1)]
    for i in range(len(allKmers)):
        if(HammingDistance(allKmers[i],Text) <= d):
            positions.append(i)
    return positions

def ApproximatePatternCount(Text, Pattern, d):
    # print(Text)
    # print(Pattern)
    # print(d)
    count = 0
    allKmers = [Pattern[i:i+len(Text)] for i in range(len(Pattern)-len(Text)+1)]
    
    for i in range(len(allKmers)):
        if(HammingDistance(allKmers[i],Text) <= d):
            count+=1
    return count
# print(ApproximatePatternCount('GCG','TGCGCGAGCGC',0))
# print(ApproximatePatternCount('GCG','TGCGCGAGCGC',1))
# print(ApproximatePatternCount('GCG','TGCGCGAGCGC',2))


def FrequentWordsWithMismatches(Text, k, d):
    allPossibleKmers = (''.join(i) for i in product('ATGC', repeat = k))
    output = []
    freqD ={}
    allKmers = [Text[i:i+k] for i in range(len(Text)-k+1)]
    for element in allPossibleKmers:
        for kmer in allKmers:
            if(HammingDistance(kmer,element) <= d):
                output.append(element)
    for element in output:
        if element in freqD:
            freqD[element] += 1
        else: 
            freqD[element] =1
    max =0
    for element in freqD:
        if freqD[element] >max:
            max = freqD[element]
    return [element for element in freqD if freqD[element] == max]

print(FrequentWordsWithMismatches('AATAATAGAATAAG',2,1))



def FrequentWordsWithMismatchesAndReverseComplements(Text, k, d):
    allPossibleKmers = (''.join(i) for i in product('ATGC', repeat = k))
    output = []
    freqD ={}
    allKmers = [Text[i:i+k] for i in range(len(Text)-k+1)]
    revComplement = week1.ReverseComplement(Text)
    allKmers.extend([revComplement[i:i+k] for i in range(len(revComplement)-k+1)])
    for element in allPossibleKmers:
        for kmer in allKmers:
            if(HammingDistance(kmer,element) <= d):
                output.append(element)
    for element in output:
        if element in freqD:
            freqD[element] += 1
        else: 
            freqD[element] =1
    max =0
    for element in freqD:
        if freqD[element] >max:
            max = freqD[element]
    return [element for element in freqD if freqD[element] == max]



# print(FrequentWordsWithMismatches('AAT',3,0))
# print (ApproximatePatternCount('AA','TACGCATTACAAAGCACA',1))
# print(HammingDistance('CAGAAAGGAAGGTCCCCATACACCGACGCACCAGTTTA','CACGCCGTATGCATAAACGAGCCGCACGAACCAGAGAG'))
# print(MinimumSkew('CATTCCAGTACTTCATGATGGCGTGAAGA'))

# print(FrequentWordsWithMismatchesAndReverseComplements('ACGTTGCATGTCGCATGATGCATGAGAGCT',4,1))

#Code for Quiz problem
def DNeighborhood(Pattern,d):
    allPossibleKmers = (''.join(i) for i in product('ATGC', repeat = 4))
    return len([element for element in allPossibleKmers if HammingDistance(element, Pattern) <=d])

# print (DNeighborhood('ACGT',3))


def ApproximatePatternCount1(Text, Pattern, d):
    allKmers = [Pattern[i:i+len(Text)] for i in range(len(Pattern)-len(Text)+1)]
    return len([kmer for kmer in allKmers if(HammingDistance(kmer,Text) <= d) ])

# print(ApproximatePatternCount1('GAGG','TTTAGAGCCTTCAGAGG',2))



# TAGGGCCGGAAGTCCCCAAT
    








