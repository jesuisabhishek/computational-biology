# Write your MotifEnumeration() function here along with any subroutines you need.
# This function should return a list of strings.
from itertools import product

def HammingDistance(p, q):
    hammingDistance =0
    for i in range(len(p)):
        if p[i] != q[i]:
            hammingDistance += 1
    return hammingDistance


def c(string,Pattern,d):
        allKmers = [string[i:i+len(Pattern)] for i in range(len(string)-len(Pattern)+1)]
        for kmer in allKmers:
            if HammingDistance(kmer,Pattern) <=d:
                return True
    


def checker(dna , Pattern,d ):
    ret =[]
    for string in dna:
        ret.append(c(string , Pattern ,d))
    return all(ret)
       
def MotifEnumeration(dna, k, d):
    pattern =[]
    for string in dna: 
        allKmers = [string[i:i+k] for i in range(len(string)-k+1)]
        
        allPossibleKmers = [''.join(i) for i in product('ATGC', repeat = k)]
        for kmer in allKmers:
            allPatternsAtMax_D_distance = [text for text in allPossibleKmers if HammingDistance(kmer,text) <=d]
            # print ("asser",kmer,allPatternsAtMax_D_distance)
            for patternAtDistance_d in allPatternsAtMax_D_distance:
                if checker(dna,patternAtDistance_d,d):
                    pattern.append(patternAtDistance_d)
    return list(set(pattern))



# Write your MedianString() function here, along with any subroutines that you need.
# You should return your answer as a string.

import math




def d(kmer,dna,k):
    distance =0
    for string in dna:
        minDist =math.inf
        allKmersInString = [string[i:i+k] for i in range(len(string) -k+1)] 
        for pattern in allKmersInString:
            if(HammingDistance(kmer,pattern)<minDist):
                minDist = HammingDistance(kmer,pattern)
        distance+=minDist  
        
    return distance    
        


def MedianString(dna, k):
    distance =math.inf
    kmers = [''.join(i) for i in product('ATGC', repeat = k)]
    Median =''
    
    for kmer in kmers:
        if distance > d(kmer, dna,k):
            distance = d(kmer, dna,k)
            Median = kmer
    return Median
# print(MedianString(['AAATTGACGCAT','GACGACCACGTT','CGTCAGCGCCTG','GCTGAGCACCGG','AGTACGGGACAG'],3))
# print(d('AAA',['TTACCTTAAC','GATATCTGTC','ACGGCGTTCG','CCCTAAAGAG','CGTCAGAGGT'],3))
            
        


def ProfileMostProbableKmer(text, k,profile):
    allKmersInString = [text[i:i+k] for i in range(len(text) -k+1)]
    maxProb =0
    toRet =''
    for kmer in allKmersInString:
        kmerProb = 1
        for i in range(k):
            kmerProb *= profile[kmer[i]][i]
        if kmerProb > maxProb:
            maxProb = kmerProb
            toRet = kmer
    if toRet == '':
        return text[:k]
    else:
        return toRet

    

def ConstructProfile(motifs,k):
    profile = {
        'A':[0] * k,
        'C':[0] * k,
        'G':[0] * k,
        'T':[0] * k,
    }
    for string in motifs:
        for i in range(len(string)):
            profile[string[i]][i] += 1
            profile[string[i]][i] += 1/2*len(motifs)
    return profile



def GreedyMotifSearch(Dna, k, t):
        BestMotifs  =[]
        for string in Dna:
            BestMotifs.append(string[:k])
        text =Dna[0]
        allKmersInString = [text[i:i+k] for i in range(len(text) -k+1)]
        for kmer  in allKmersInString:
            Motifs =  [kmer]
            for i in range(1,t):
                profile = ConstructProfile(Motifs,k)
                Motifs.append(ProfileMostProbableKmer(Dna[i],k,profile)) 
            if Score(Motifs,k) < Score(BestMotifs,k):
                BestMotifs = Motifs
        return BestMotifs


def Score(Motif,k):
    # print (Motif)
    # print(k)
    MotifScore =0
    for i in range(k):
        colScore =0
        col= []
        for j in range(len(Motif)):
            col.append(Motif[j][i])
        freq = {}
        for c in col:
            if c in freq:
                freq[c]+=1
            else:
                freq[c]=1
        maxFreq = 0
        freqCharacter =''
        for c in freq:
            if freq[c] >maxFreq:
                maxFreq = freq[c]
                freqCharacter = c
        for c in freq:
            if not  c == freqCharacter:
                colScore+=freq[c]
        MotifScore+=colScore
    return MotifScore

def ConstructProfileWithPseudoCount(motifs,k,pseudocount):
    profile = {
        'A':[pseudocount] * k,
        'C':[pseudocount] * k,
        'G':[pseudocount] * k,
        'T':[pseudocount] * k,
    }
    for string in motifs:
        for i in range(len(string)):
            profile[string[i]][i] += 1/len(motifs)+k
    return profile
         

def GreedyMotifSearchWithPseudocounts(Dna, k, t, pseudocount):
        BestMotifs  =[]
        for string in Dna:
            BestMotifs.append(string[:k])
        text =Dna[0]
        allKmersInString = [text[i:i+k] for i in range(len(text) -k+1)]
        for kmer  in allKmersInString:
            Motifs =  [kmer]
            for i in range(1,t):
                profile = ConstructProfileWithPseudoCount(Motifs,k,pseudocount)
                Motifs.append(ProfileMostProbableKmer(Dna[i],k,profile)) 
            if Score(Motifs,k) < Score(BestMotifs,k):
                BestMotifs = Motifs
        return BestMotifs

# print(ProfileMostProbableKmer('ACCTGTTTATTGCCTAAGTTCCGAACAAACCCAATATAGCCCGAGGGCCT',5,{
#     'A': [0.2, 0.2, 0.3, 0.2, 0.3], 
#     'C': [0.4, 0.3, 0.1, 0.5, 0.1], 
#     'G': [0.3, 0.3, 0.5, 0.2, 0.4], 
#     'T': [0.1, 0.2, 0.1, 0.1, 0.2]}))

# GreedyMotifSearch(['GGCGTTCAGGCA','AAGAATCAGTCA','CAAGGAGTTCGC','CACGTCAATCAC','CAATAATATTCG'],3,5)
# print(GreedyMotifSearch(['GAGGCGCACATCATTATCGATAACGATTCGCCGCATTGCC','TCATCGAATCCGATAACTGACACCTGCTCTGGCACCGCTC','TCGGCGGTATAGCCAGAAAGCGTAGTGCCAATAATTTCCT','GAGTCGTGGTGAAGTGTGGGTTATGGGGAAAGGCAGACTG','GACGGCAACTACGGTTACAACGCAGCAACCGAAGAATATT','TCTGTTGTTGCTAACACCGTTAAAGGCGGCGACGGCAACT','AAGCGGCCAACGTAGGCGCGGCTTGGCATCTCGGTGTGTG','AATTGAAAGGCGCATCTTACTCTTTTCGCTTTCAAAAAAA'],5,8))
# print(GreedyMotifSearch(['GAGGCGCACATCATTATCGATAACGATTCGCCGCATTGCC','TCATCGAATCCGATAACTGACACCTGCTCTGGCACCGCTC','TCGGCGGTATAGCCAGAAAGCGTAGTGCCAATAATTTCCT','GAGTCGTGGTGAAGTGTGGGTTATGGGGAAAGGCAGACTG','GACGGCAACTACGGTTACAACGCAGCAACCGAAGAATATT','TCTGTTGTTGCTAACACCGTTAAAGGCGGCGACGGCAACT','AAGCGGCCAACGTAGGCGCGGCTTGGCATCTCGGTGTGTG','AATTGAAAGGCGCATCTTACTCTTTTCGCTTTCAAAAAAA'],5,8))
