def PatternMatching(Pattern, Genome):
    ans =[]
    for i in range(len(Genome)):
        if ( i+len(Pattern) <= len(Genome) and Genome[i:i+len(Pattern)] == Pattern):
            ans.append(i)
    return ans


def ClumpFinding(genome, k, L, t):
    output = []
    for i in range(len(genome) -L+1):
            genome_subsection = genome[i:i+L]
            frequency ={}
            for j in range(len(genome_subsection)-k+1):
                kmer = genome_subsection[j:j+k]
                if kmer in frequency:
                    frequency[kmer]+=1
                else: 
                    frequency[kmer] =1    
            output.extend([element for element in frequency if frequency[element]>=t])
            output = list(set(output))
    return list(set(output))

def ReverseComplement(Pattern):
    reverse =''
    for c in Pattern:
        if c == 'A':
            reverse+= 'T'
        elif c =='T':
            reverse+= 'A'
        elif c =='G':
            reverse+= 'C'
        elif c =='C':
            reverse+= 'G'
    reverse =reverse[::-1]
    return reverse

def PatternCount(Text, Pattern):
    count  =  0
    for i in range(0 , len(Text) - len(Pattern)):
            if Text[i:len(Pattern)] == Pattern:
                count += 1
    return count


def FrequentWords(Text, k):
    l =[]
    d ={}
    max =0
    maxelements = []
    for c in range(len(Text)):
        if c+k <=len(Text):
            l.append(Text[c:c+k])
    for element in l:
        if element in d:
            d[element] += 1
        else:
            d[element] = 1

    for element in d:
        if d[element] > max:
            max =d[element]
    for element in d:
        if d[element] == max:
            maxelements.append(element)
    print (maxelements)
    return maxelements





def StartingPositions(Genome,Pattern):
    output =[]
    for i in range(len(Genome)-len(Pattern)+1):
        if Genome[i:i+len(Pattern)] ==Pattern:
            output.append(i)
    return output
# FrequentWords('CGGAGGACTCTAGGTAACGCTTATCAGGTCCATAGGACATTCA',3)
# print (ReverseComplement('TTGTGTC'))
# print(StartingPositions('ATGACTTCGCTGTTACGCGC','CGC'))
# print(ReverseComplement('ATGCAT'))
# print(ReverseComplement('ATGCAT'))










